# ExampleFortranDoxygen

Example of fortran project with documentation published on GitLab Pages website. The documentation is generated from source code with [doxygen](https://www.doxygen.nl/index.html).

## How it works
Both code and documentation are built with CMake. The documentation is generated with doxygen.
The script **.gitlab-ci.yml** initiates remote build of documentation and deploys result to Pages. [The result looks pretty](https://vonopr.gitlab.io/example-fortran-doxygen) due to [doxygen-bootstrapped theme](https://github.com/Velron/doxygen-bootstrapped).

## Installation

Check [dependencies](#dependencies) and clone the repository to install it locally. The example of how to build and install the project from an empty directory:

```console
git clone https://gitlab.com/vonopr/example-fortran-doxygen.git source
mkdir build
mkdir installed
cd build
cmake -S../source -B. -DCMAKE_INSTALL_PREFIX=../installed
cmake --build . 
cmake --build . --target html
cmake --install .
```

### Dependencies

 - gfortran (or other modern fortran compiler)
 - CMake 3.13+
 - Doxygen

## Configuring Doxygen

Doxygen is configured with [doxygen_add_docs() cmake function](https://cmake.org/cmake/help/latest/module/FindDoxygen.html#command:doxygen_add_docs).

Doxygen options can be set with correspondent `DOXYGEN_` cmake variables. To confiure doxygen with `EXTRACT_ALL = YES`  set a `DOXYGEN_EXTRACT_ALL` cmake variable with
```cmake
set(DOXYGEN_EXTRACT_ALL YES)
```

## Doxygen Bootstrapped Theme

[Original Doxygen Bootstrapped Theme](https://github.com/Velron/doxygen-bootstrapped.git) does not work with doxygen versions 1.8.12 and newer. I use [a fork that works with new doxygen versions](https://github.com/OlivierLDff/DoxygenBootstrapped), at least up to doxygen-1.8.18.

Files of Doxygen Bootstrapped Theme are obtained during build with `ExternalProject_Add()` cmake function.

## Use README.md as documenation main page

Using `README.md` as documenation main page must have been as easy as setting a cmake variable `DOXYGEN_USE_MDFILE_AS_MAINPAGE` with
```cmake
set(DOXYGEN_USE_MDFILE_AS_MAINPAGE README.md)
```
and passing `README.md` to `doxygen_add_docs()` as the first input file.
```cmake
doxygen_add_docs(
    doxygen
    ${PROJECT_SOURCE_DIR}/README.md ${PROJECT_SOURCE_DIR}/src
    COMMENT "Generate documentation"
)
```

The problem is that doxygen produces an ugly warning message in the beginning of the documentation main page.
To avoid it one may place a doxygen \\*mainpage* tag in the beginning of `README.md` or
append `{#mainpage}` to the line with main title. Both look ugly on gitlab repository website.

My next guess was to create a separate doxygen main page file and include `README.md` in it.
Unfortunately, [this was not possible due to markdown file could be included as a palin text only](https://github.com/doxygen/doxygen/issues/7688).

My solution is to preprocess `README.md` using cmake. I added to `doc/CMakeLists.txt` the following
```cmake
file(WRITE ${CMAKE_CURRENT_BINARY_DIR}/mainpage.md "")
file(STRINGS "${PROJECT_SOURCE_DIR}/README.md" lines)
foreach(line IN LISTS lines)
  string(APPEND line " ")
  string(REGEX REPLACE "^# (.+)$" "# \\1  {#mainpage}" line_parsed ${line})
  string(APPEND line_parsed "\n")
  file(APPEND ${CMAKE_CURRENT_BINARY_DIR}/mainpage.md ${line_parsed})
endforeach()
```
The line ```string(REGEX REPLACE "^# (.+)$" "# \\1  {#mainpage}" line_parsed ${line})``` adds `{#mainpage}` to every line started with hash `#` immediately followed by space.
A line ```string(APPEND line " ")``` guarantees the last argument of `string(REGEX REPLACE ...)` is not an empty string which would cause an error. 


## Extend to other lanuages

The project is not much Fortran-specific. It is easy to extend for any [language supported by doxygen](https://en.wikipedia.org/wiki/Doxygen#Uses). You just need to set a compiler and add a langauge optimization option to doxygen configuration.

### How to use with C

To enable documenation of C code add the following line into `doc/CMakeLists.txt`
```cmake
set(DOXYGEN_OPTIMIZE_FOR_C YES)
```
and put your C source code files into `src` directory.

Add a C compiler with the following line in `CMakeLists.txt`
```cmake
project ("ExampleFortranDoxygen" LANGUAGES C Fortran)
```

Add new targets to `src/CMakeLists.txt`. The example below is for sources `foo.c`, `foo.h` and `main.c` 
```cmake
add_library(myclib foo.c)
add_executable(cmain main.c)
target_link_libraries(cmain PUBLIC myclib)
install(TARGETS myclib cmain)
```
