!> A say hello module
module fhello_mod

contains

!> A say hello subroutine
  subroutine fhello()
    write(*,"(A)") "Hello, World!"
  end subroutine


end module
