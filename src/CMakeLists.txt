add_library(fhello f_hello.f90)
add_executable(fmain f_main.f90)
target_link_libraries(fmain PUBLIC fhello)

install(TARGETS fhello fmain
       ARCHIVE DESTINATION lib
       LIBRARY DESTINATION lib
       RUNTIME DESTINATION bin)